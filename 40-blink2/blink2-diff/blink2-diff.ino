void setup(){  // runs once
  pinMode(13, OUTPUT);  // set up pin 13 for output
  pinMode(12, OUTPUT);  // set up pin 12 for output
}

void loop(){ // repeats forever
  digitalWrite(13, HIGH);  // set pin 13 to 5V
  digitalWrite(12, HIGH);  // set pin 12 to 5V
  delay(250);  // wait 250 ms = 0.25 s
  digitalWrite(12, LOW); // set pin 12 to 0V
  delay(250);  // wait 250 ms
  digitalWrite(12, HIGH); // set pin 12 to 5V
  delay(250);  // wait 250 ms
  digitalWrite(13, LOW); // set pin 13 to 0V
  digitalWrite(12, LOW); // set pin 12 to 0 V
  delay(250);  
}
