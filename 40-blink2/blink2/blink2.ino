// Blink two LEDS in sync

void setup(){ // runs once
  pinMode(13, OUTPUT); // set up pin 13 for output
  pinMode(12, OUTPUT); // set up pin 12 for output
}

void loop(){ // repeats forever
  digitalWrite(13, HIGH); // set pin 13 to 5V
  digitalWrite(12, HIGH); // set pin 12 to 5V
  delay(1000);  // wait 1000 ms = 1 second
  digitalWrite(12, LOW); // set pin 12 to 0 V
  digitalWrite(13, LOW); // set pin 13 to 0 V
  delay(1000);  // wait 1000 ms
}
