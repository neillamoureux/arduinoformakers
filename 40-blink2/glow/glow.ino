// make an LED fade in and out

// constants
// Constants make it easier to change the program
// For example, if you decide to use another pin for the
// LED, you only need to change the next line.
int ledPin = 9; // the pin to which the LED is attached
int wait = 30; // time to wait in ms between changes to brightness 
int minBrightness = 0; // darkest level; cannot be less than 0
int maxBrightness = 255; // brightest; cannot be more than 255


int brightness = 0; // this changes to set the brightness
int changeBy = 10; // amount to change level by each loop

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT); // set up pin for output  
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(ledPin, brightness); // set brightness of LED
  delay(wait); // wait 30 ms
  brightness = brightness + changeBy; // or brightness += changeBy;
  
  // Once we hit max brightness, reverse change in brightness for each step
  // so we start to fade 
  if (brightness > maxBrightness){ 
      brightness = maxBrightness;
      changeBy = -changeBy;
  }
  // once we hit min brightness, reverse change so we start getting
  // brighter again
  if (brightness < minBrightness){
      brightness = minBrightness;
      changeBy = -changeBy;
  }
}
