// Turn a speaker on and off 

// Constants
const int SPEAKER_PIN = 4;
const int WAIT = 100; // how long to wait between steps
const int MIN_FREQ = 220; // low A
const int MAX_FREQ = 880; // high A
const int STEP_COUNT = 20; // how much to step by

// current frequency
int freq;
// size of each step
float stepSize;

void setup() {
  // put your setup code here, to run once:
  freq = MIN_FREQ;
  // calculet the size of a step
  stepSize = (MAX_FREQ - MIN_FREQ) / STEP_COUNT;
}

void loop() {
  // put your main code here, to run repeatedly:
  freq += stepSize; // this is same as freq = freq + stepSize
  if (freq > MAX_FREQ){
      freq = MAX_FREQ;
      stepSize = -stepSize;
  }
  if (freq < MIN_FREQ){
      freq = MIN_FREQ;
      stepSize = -stepSize;
  }
  tone(SPEAKER_PIN, freq);
  delay(WAIT);
}
