// Turn a speaker on and off 

// Constants
const int SPEAKER_PIN = 4;
const int DELAY_MS = 2;

void setup() {
  // put your setup code here, to run once:
  pinMode(SPEAKER_PIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(SPEAKER_PIN, HIGH);
  delay(DELAY_MS);
  digitalWrite(SPEAKER_PIN, LOW);
  delay(DELAY_MS);
}
