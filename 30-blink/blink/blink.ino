// Blink an LED, once per second

void setup(){  // runs once at start
  pinMode(13, OUTPUT); // set up pin 13 for output
}

void loop(){ // repeats forever
  digitalWrite(13, HIGH); // set pin 13 to 5V
  delay(1000);  // wait 1000 ms = 1 second
  digitalWrite(13, LOW); // set pin 13 to 0 V
  delay(1000);  // wait 1000 ms
}
