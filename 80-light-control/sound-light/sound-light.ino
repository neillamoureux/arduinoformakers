//Control pitch with light sensor

//Constants
const int SPEAKER_PIN = 4;
const int SENSOR_PIN = 0;
const int DELAY = 30;
const int MIN_FREQ = 220;
const int MAX_FREQ = 880;
const int MIN_SENSOR = 480;
const int MAX_SENSOR = 920;
const int WAIT = 100; //ms
const int SERIAL_PORT_RATE = 9600;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(SERIAL_PORT_RATE); // for debugging
}

void loop() {
  // put your main code here, to run repeatedly:
  int light = analogRead(SENSOR_PIN);
  Serial.println(light);
  int freq = map(light, MIN_SENSOR, MAX_SENSOR, MIN_FREQ, MAX_FREQ);
  tone(SPEAKER_PIN, freq);
  delay(WAIT); 
}
