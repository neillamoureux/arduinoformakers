//Control pitch with light sensor

//Constants
const int SPEAKER_PIN = 4;
const int SENSOR_PIN = 0;
const int DELAY = 30;
const int MIN_SENSOR = 0;
const int MAX_SENSOR = 1023;
const int WAIT = 100; //ms
const int PIN_SWITCH_1 = 5;
const int PIN_SWITCH_2 = 6;
const int PIN_SWITCH_3 = 7;

const int MIN_FREQ_DEFAULT = 600;
const int MAX_FREQ_DEFAULT = 660;
const int MIN_FREQ_1 = 110;
const int MAX_FREQ_1 = 220;
const int MIN_FREQ_2 = 220;
const int MAX_FREQ_2 = 440;
const int MIN_FREQ_3 = 440;
const int MAX_FREQ_3 = 880;

const int SERIAL_PORT_RATE = 9600;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(SERIAL_PORT_RATE); // for debugging
  pinMode(PIN_SWITCH_1, INPUT_PULLUP);
  pinMode(PIN_SWITCH_2, INPUT_PULLUP);
  pinMode(PIN_SWITCH_3, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  int light = analogRead(SENSOR_PIN);
  Serial.println(light);
  // determine the freq range based on the switches
  int minFreq = 10000;
  int maxFreq = -1;
  
  if (digitalRead(PIN_SWITCH_1) == LOW){
      minFreq = min(minFreq, MIN_FREQ_1);
      maxFreq = max(maxFreq, MAX_FREQ_1);
  }
  if (digitalRead(PIN_SWITCH_2) == LOW){
      minFreq = min(minFreq, MIN_FREQ_2);
      maxFreq = max(maxFreq, MAX_FREQ_2);
  }
  if (digitalRead(PIN_SWITCH_3) == LOW){
      minFreq = min(minFreq, MIN_FREQ_3);
      maxFreq = max(maxFreq, MAX_FREQ_3);
  }
  
  if (maxFreq == -1){
    minFreq = MIN_FREQ_DEFAULT;
    maxFreq = MAX_FREQ_DEFAULT;
  }
  Serial.print("min:");
  Serial.println(minFreq);  
  Serial.print("max:");
  Serial.println(maxFreq);  
  int freq = map(light, MIN_SENSOR, MAX_SENSOR, minFreq, maxFreq);
  Serial.print("freq:");
  Serial.println(freq);  
  tone(SPEAKER_PIN, freq);
  delay(WAIT); 
}
